package org.brucalipto.gdrivecmd;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleOAuthConstants;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.File.Labels;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.ParentReference;

public class GDriveCMD 
{
	private final static Log log = LogFactory.getLog(Configuration.class);
	
	public static void main(String[] args) throws Exception 
	{
		CommandLineArgs.init(args); 
		new GDriveCMD();
	}
	
	private GDriveCMD() throws Exception 
	{
		Util.setupProxy();
		
		HttpTransport httpTransport = new NetHttpTransport();
		JsonFactory jsonFactory = new JacksonFactory();

		Configuration conf = Configuration.getInstance();
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
				httpTransport, jsonFactory, conf.getProperty(Configuration.CLIENT_ID), conf.getProperty(Configuration.CLIENT_SECRET),
				Arrays.asList(DriveScopes.DRIVE)).setAccessType("online")
				.setApprovalPrompt("auto").build();

		String url = flow.newAuthorizationUrl().setRedirectUri(GoogleOAuthConstants.OOB_REDIRECT_URI).build();
		log.info("Please open the following URL in your browser then type the authorization code:");
		log.info("  " + url);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String code = br.readLine();

		GoogleTokenResponse response = flow.newTokenRequest(code).setRedirectUri(GoogleOAuthConstants.OOB_REDIRECT_URI).execute();
		GoogleCredential credential = new GoogleCredential().setFromTokenResponse(response);

		// Create a new authorized API client
		Drive service = new Drive.Builder(httpTransport, jsonFactory, credential).setApplicationName("GDrive Uploader").build();
		CommandLineArgs cla = CommandLineArgs.getInstance();
		String command = cla.getJc().getParsedCommand();
		if (CommandLineArgs.ListCommand.COMMAND.equals(command))
		{
			log.info("Going to list files...");
			listFiles(service);
		}
		else if (CommandLineArgs.AddCommand.COMMAND.equals(command))
		{
			log.info("Going to add file...");
			addFile(service);
		}
		else if (CommandLineArgs.DownloadCommand.COMMAND.equals(command))
		{
			log.info("Going to download file...");
			downloadFile(service);
		}
		else
		{
			throw new IllegalArgumentException("Command '"+command+"' is unknown!");
		}
	}
	
	private void downloadFile(Drive service) throws Exception
	{
		File file = service.files().get(CommandLineArgs.DOWNLOAD_COMMAND.fileId).execute();
		String description = (file.getDescription()==null)?file.getTitle():file.getDescription();
		String downloadURL = file.getDownloadUrl();
		if (CommandLineArgs.DOWNLOAD_COMMAND.exportAsPDF)
		{
			downloadURL = file.getExportLinks().get("application/pdf");
			description += ".pdf";
		}
		
		log.info("Going to get file '"+description+"'");
		if (file.getFileSize()!=null)
		{
			log.info("Filesize: '"+NumberFormat.getInstance().format(file.getFileSize())+"' bytes");
		}
		log.debug("Going to download file '"+description+"' from '"+downloadURL+"'");
		
		if (downloadURL!=null && downloadURL.length()>0) 
		{
			HttpResponse resp = service.getRequestFactory().buildGetRequest(new GenericUrl(downloadURL)).execute();
			resp.download(new FileOutputStream(description));
		}
	}
	
	private void listFiles(Drive service) throws Exception
	{
		FileList fileList = service.files().list().execute();
		if (CommandLineArgs.LIST_COMMAND.allDetails)
		{
			log.info(fileList.toPrettyString());
		}
		else
		{
			log.info(StringUtils.leftPad("", 80, '-'));
			log.info("|           File ID            |       ParentID               |  Description |");
			log.info(StringUtils.leftPad("", 80, '-'));
			List<File> list = fileList.getItems();
			for (File file : list)
			{
				String mimeType = file.getMimeType();
				if (CommandLineArgs.LIST_COMMAND.onlyFolders && !"application/vnd.google-apps.folder".equals(mimeType))
				{
					continue;
				}
				String id = StringUtils.leftPad(file.getId(), 44);
				StringBuilder sb = new StringBuilder();
				List<ParentReference> parents = file.getParents();
				for (ParentReference pr : parents)
				{
					sb.append(pr.getId());//.append(" ("+pr.getKind()+")");//.append(System.getProperty("line.separator"));
				}
				String description = (file.getDescription()==null)?file.getTitle():file.getDescription();
				
				StringBuilder row = new StringBuilder();
				row.append("| ").append(id).append(" | ").append(StringUtils.leftPad(sb.toString(), 61)).append(" | ");
				row.append(StringUtils.leftPad(StringUtils.substring(description, 0, 30), 32)).append(" |");
				log.info(row);
			}
			log.info(StringUtils.leftPad("", 80, '-'));
		}
	}
	
	private void addFile(Drive service) throws Exception
	{
		String filename = CommandLineArgs.ADD_COMMAND.filename;
		
		java.io.File fileContent = new java.io.File(filename);
		if (!fileContent.exists())
		{
			log.fatal("File '"+filename+"' does not exists!");
			System.exit(-1);
		}
		log.info("Going to upload file '"+filename+"'");
		String mimeType = Util.getMimeType(filename);
		log.info("Detected mimeType: '"+mimeType+"'");
		FileContent mediaContent = new FileContent(mimeType, fileContent);
		
		// Insert a file
		File body = new File();
		if (StringUtils.isNotEmpty(CommandLineArgs.ADD_COMMAND.title))
		{
			log.debug("Setting title to '"+CommandLineArgs.ADD_COMMAND.title+"'");
			body.setTitle(CommandLineArgs.ADD_COMMAND.title);
		}
		else
		{
			if (CommandLineArgs.ADD_COMMAND.defaultTitle)
			{
				log.debug("Setting title to '"+fileContent.getName()+"'");
				body.setTitle(fileContent.getName());
			}
		}
		if (StringUtils.isNotEmpty(CommandLineArgs.ADD_COMMAND.description))
		{
			log.debug("Setting description to '"+CommandLineArgs.ADD_COMMAND.description+"'");
			body.setDescription(CommandLineArgs.ADD_COMMAND.description);
		}
		else
		{
			if (CommandLineArgs.ADD_COMMAND.defaultDescription)
			{
				log.debug("Setting description to '"+fileContent.getName()+"'");
				body.setDescription(fileContent.getName());
			}
		}
		body.setMimeType(mimeType);
		Labels labels = new Labels();
		body.setLabels(labels);
		
		if (StringUtils.isNotEmpty(CommandLineArgs.ADD_COMMAND.parentFolderId))
		{
			log.debug("Setting parentFolderId to '"+CommandLineArgs.ADD_COMMAND.parentFolderId+"'");
			ParentReference pr = new ParentReference();
			pr.setId(CommandLineArgs.ADD_COMMAND.parentFolderId);
			List<ParentReference> parents = new ArrayList<ParentReference>();
			parents.add(pr);
			body.setParents(parents);
		}

		File file = service.files().insert(body, mediaContent).execute();
		log.info("File ID: " + file.getId());
		log.info("Server response: '"+file.toPrettyString()+"'");
	}
}