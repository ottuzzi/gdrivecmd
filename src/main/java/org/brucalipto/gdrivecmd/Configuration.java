package org.brucalipto.gdrivecmd;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Configuration 
{
	public final static String CLIENT_ID = "google.clientId";
	public final static String CLIENT_SECRET = "google.clientSecret";
	public final static String PROXY_HOST = "proxy.host";
	public final static String PROXY_PORT = "proxy.port";
	public final static String PROXY_USER = "proxy.user";
	public final static String PROXY_PASSWD = "proxy.password";
	
	private final static Log log = LogFactory.getLog(Configuration.class);
	private final static Configuration configuration = new Configuration();
	
	private final Properties properties = new Properties(); 
	
	public static Configuration getInstance()
	{
		return configuration;
	}
	
	private Configuration()
	{
		try 
		{
			properties.load(this.getClass().getResourceAsStream("/config.properties"));
			log.debug("Configuration loaded: '"+properties.toString()+"'");
		} 
		catch (Exception e) 
		{
			log.fatal("Error loading configuration", e);
			System.exit(-1);
		}
	}
	
	public String getProperty(String key)
	{
		String property = properties.getProperty(key);
		return (property==null)?null:property.trim();
	}
}
