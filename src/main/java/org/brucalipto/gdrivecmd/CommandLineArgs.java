package org.brucalipto.gdrivecmd;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

public class CommandLineArgs 
{
	private final static Log log = LogFactory.getLog(CommandLineArgs.class);
	private static CommandLineArgs instance;
	
	public final static AddCommand ADD_COMMAND = new AddCommand();
	public final static ListCommand LIST_COMMAND = new ListCommand();
	public final static DownloadCommand DOWNLOAD_COMMAND = new DownloadCommand();
	
	private final JCommander jc = new JCommander();
	
	public JCommander getJc() 
	{
		return jc;
	}

	private CommandLineArgs(String[] args)
	{
		jc.setProgramName("GDriveCMD");
		jc.addCommand(ListCommand.COMMAND, LIST_COMMAND);
		jc.addCommand(AddCommand.COMMAND, ADD_COMMAND);
		jc.addCommand(DownloadCommand.COMMAND, DOWNLOAD_COMMAND);
		
		try 
		{	
			jc.parse(args);
		} 
		catch (Exception e) 
		{
			log.debug("Caught Exception!", e);
			log.fatal("Error parsing command line");
			jc.usage();
			System.exit(-1);
		}
		String command = jc.getParsedCommand();
		if (ListCommand.COMMAND.equals(command) || AddCommand.COMMAND.equals(command) || DownloadCommand.COMMAND.equals(command))
		{
			
		}
		else
		{
			if (StringUtils.isEmpty(command))
			{
				log.fatal("A command is needed to start application");
			}
			else
			{
				log.fatal("Command '"+command+"' unknown");
			}
			jc.usage();
			System.exit(-1);
		}
	}
	
	public static CommandLineArgs init(String[] args)
	{
		instance = new CommandLineArgs(args);
		return instance;
	}
	
	public static CommandLineArgs getInstance()
	{
		return instance;
	}
	
	public final static class ListCommand 
	{
		public final static String COMMAND = "list";
		
		@Parameter(names = "--list", description = "List files and details")
		public Boolean l = false;
		
		@Parameter(names = "--allDetails", description = "List complete JSON")
		public Boolean allDetails = false;
		
		@Parameter(names = "--onlyFolders", description = "List only folders")
		public Boolean onlyFolders = false;
	}
	
	@Parameters(separators = "=", commandDescription = "Add a document to your Google DRIVE")
	public final static class AddCommand 
	{
		public final static String COMMAND = "add";
		
		@Parameter(names = "--filename", required = true, description = "The file to add")
		public String filename;
		
		@Parameter(names = "--title", description = "Document title")
		public String title;
		
		@Parameter(names = "--defaultTitle", description = "Set document title the same as filename")
		public Boolean defaultTitle = false;
		
		@Parameter(names = "--description", description = "Document description")
		public String description;
		
		@Parameter(names = "--defaultDescription", description = "Set document description the same as filename")
		public Boolean defaultDescription = false;
		
		@Parameter(names = "--parentFolderId", description = "Document parent folder")
		public String parentFolderId;
	}
	
	@Parameters(separators = "=", commandDescription = "Download document from your Google DRIVE")
	public final static class DownloadCommand 
	{
		public final static String COMMAND = "download";
		
		@Parameter(names = "--fileid", required = true, description = "The file to download")
		public String fileId;
		
		@Parameter(names = "--exportAsPDF", description = "Save document as PDF")
		public Boolean exportAsPDF = false;
	}
}
