package org.brucalipto.gdrivecmd;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

import org.apache.commons.lang3.StringUtils;

import eu.medsea.mimeutil.MimeUtil2;

public class Util 
{
	public static String getMimeType(String filename)
	{
		MimeUtil2 mimeUtil = new MimeUtil2();
		mimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
		return MimeUtil2.getMostSpecificMimeType(mimeUtil.getMimeTypes(filename)).toString();
	}
	
	public static void setupProxy()
	{
		Configuration conf = Configuration.getInstance();
		
		final String proxyHost = conf.getProperty(Configuration.PROXY_HOST);
		final String proxyPort = conf.getProperty(Configuration.PROXY_PORT);
		final String proxyUser = conf.getProperty(Configuration.PROXY_USER);
		final String proxyPassword = conf.getProperty(Configuration.PROXY_PASSWD);

		if (StringUtils.isNotEmpty(proxyHost))
		{
			System.setProperty("http.proxyHost", proxyHost);
			System.setProperty("https.proxyHost", proxyHost);
		}
		
		if (StringUtils.isNotEmpty(proxyPort))
		{
			System.setProperty("http.proxyPort", proxyPort);
			System.setProperty("https.proxyPort", proxyPort);
		}
		
		if (StringUtils.isNotEmpty(proxyUser))
		{
			System.setProperty("http.proxyUser", proxyUser);
			System.setProperty("https.proxyUser", proxyUser);
		}
		if (StringUtils.isNotEmpty(proxyPassword))
		{
			System.setProperty("http.proxyPassword", proxyPassword);
			System.setProperty("https.proxyPassword", proxyPassword);
		}

		if (StringUtils.isNotEmpty(proxyUser) && StringUtils.isNotEmpty(proxyPassword))
		{
			Authenticator.setDefault(
			  new Authenticator() {
			    public PasswordAuthentication getPasswordAuthentication() {
			      return new PasswordAuthentication(proxyUser, proxyPassword.toCharArray());
			    }
			  }
			);
		}
	}
}
